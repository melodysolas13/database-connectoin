<?php

$host = 'localhost';
$user = 'root';
$pass = '';
$db   = 'learning_php';

try
{
    $db_con = new PDO("mysql:host={$host};dbname={$db}",$user, $pass);
    $db_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $ex)
{
    echo $ex->getMessage();
}



?>